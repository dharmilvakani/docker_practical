FROM node:16-slim
WORKDIR /index
COPY package.json .
RUN npm install
COPY . ./
EXPOSE 3000
CMD ["npm","run","dev"]