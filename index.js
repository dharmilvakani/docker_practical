const express = require("express")
const app = express()
const port = process.env.PORT || 3000

app.get("/", (req, res) => {
    res.send("Hello world")
})

app.get("/contact", (req, res) => {
    res.send("This is contact page")
})

app.get("/about", (req, res) => {
    res.send("This is about page")
})

app.listen(port, () => {
    console.log("Successfully Connected on port" + port)
})